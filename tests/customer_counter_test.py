import unittest

from customer_counter import counter_sum


class CustomerCounterTestCase(unittest.TestCase):

    def test_counter_sum(self):
        self.assertEqual(10, counter_sum([5, 5]))
